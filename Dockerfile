FROM registry.access.redhat.com/ubi8:latest

WORKDIR /minecraft

ADD https://piston-data.mojang.com/v1/objects/84194a2f286ef7c14ed7ce0090dba59902951553/server.jar server.jar

RUN yum -y update && yum -y install wget

RUN wget https://download.oracle.com/java/17/latest/jdk-17_linux-x64_bin.rpm && \
    yum -y install jdk-17_linux-x64_bin.rpm

RUN echo "eula=true" > eula.txt

RUN chgrp -R 0 /minecraft && \
    chmod -R g=u /minecraft

EXPOSE 25565

CMD ["java", "-Xmx2G", "-Xms1G", "-jar", "server.jar", "nogui"]